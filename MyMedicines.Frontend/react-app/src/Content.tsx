import React, { useContext, useEffect, useState, createContext } from 'react';
import { Medicine } from './Medicine';

export const MedApiContext = React.createContext<MedicinesApi | undefined>(
  undefined
);
export interface MedcineProps {
  useContextHook?: typeof useContext;
}

const Medicines: React.FC<MedcineProps> = ({
  useContextHook = useContext,
}) => {
  const medApi = useContext(MedApiContext);

  const [meds, setMeds] = useState([]); //useState<TaskTableEntry[]>([]);
};

export default function Content(){
    const medsApi = useContext(MedApiContext);

    const [data, setData] = useState([]);
    
    const makeAPICall = async () => {
        try {
        
          const response = await fetch('https://localhost:44380/api/medicines');
          debugger;
          const data = await response.json();
          //TODO map data to MEdicine object
          console.log({ data })
          setData(data)
        }
        catch (e) {
          console.log(e)
        }
      };
      
    useEffect(() => {  
        if (!medsApi) return;
        console.log(getMedicines())
    }, [medsApi]);  

    
    return (
        <div></div>
      );
};

function getMedicines(): Medicine[] {
  return  [
    {"Id":1, "Name":"Paracetamol", "ExpirationDate":new Date("12-05-2023"),"Category":"Ból", "Quantity":16,"Unit":"tablets"},
    {"Id":2, "Name":"Apap", "ExpirationDate":new Date("17-01-2021"),"Category":"Ból", "Quantity":5,"Unit":"tablets"}
  ]
}
