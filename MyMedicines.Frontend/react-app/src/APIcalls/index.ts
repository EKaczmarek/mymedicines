import * as coreClient from "@azure/core-client";

export interface Medicine {
    Id: number;
    Name: string;
    ExpirationDate: Date;
    Category: string;
    Quantity: number;
    Unit: string;
}

export type GetMedicinesResponse = Medicine[];

/** Optional parameters. */
export interface GetMedicinesOptionalParams
  extends coreClient.ServiceClientOptions {
  /** Overrides client endpoint. */
  endpoint?: string;
}


/** Optional parameters. */
export interface GetMedicinesAPIOptionalParams
  extends coreClient.OperationOptions {
    /** Any object */
    query?: Record<string, unknown>;
}


/** Optional parameters. */
export interface MedicinesAPIOptionalParams
  extends coreClient.ServiceClientOptions {
  /** Overrides client endpoint. */
  endpoint?: string;
}
