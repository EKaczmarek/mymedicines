
import {
    OperationParameter,
    OperationURLParameter,
    OperationQueryParameter
  } from "@azure/core-client";
  
export const $host: OperationURLParameter = {
    parameterPath: "$host",
    mapper: {
      serializedName: "$host",
      required: true,
      type: {
        name: "String"
      }
    },
    skipEncoding: true
  };
  
  export const query: OperationQueryParameter = {
    parameterPath: ["options", "query"],
    mapper: {
      serializedName: "query",
      type: {
        name: "Dictionary",
        value: { type: { name: "any" } }
      }
    }
  };
  

  export const accept: OperationParameter = {
    parameterPath: "accept",
    mapper: {
      defaultValue: "application/json, text/json",
      isConstant: true,
      serializedName: "Accept",
      type: {
        name: "String"
      }
    }
  };