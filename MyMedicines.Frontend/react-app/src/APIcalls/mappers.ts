import * as coreClient from "@azure/core-client";

export const MedicineDto: coreClient.CompositeMapper = {
    type: {
      name: "Composite",
      className: "MedicineDto",
      modelProperties: {
        id: {
          serializedName: "id",
          nullable: true,
          type: {
            name: "String"
          }
        },
        name: {
            serializedName: "name",
            nullable: true,
            type: {
              name: "String"
            }
          },
        expirationDate: {
          serializedName: "expirationDate",
          type: {
            name: "DateTime"
          }
        },
        category: {
          serializedName: "category",
          nullable: true,
          type: {
            name: "String"
          }
        },
        quantity: {
          serializedName: "quantity",
          nullable: true,
          type: {
            name: "Number"
          }
        },
        unit: {
            serializedName: "unit",
            nullable: true,
            type: {
              name: "String"
            }
          }
      }
    }
  };