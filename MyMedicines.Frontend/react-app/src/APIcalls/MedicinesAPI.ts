import * as coreClient from "@azure/core-client";
import * as coreAuth from "@azure/core-auth";
import * as Parameters from "./parameters";
import * as Mappers from "./mappers";
import { MedicinesAPIOptionalParams, GetMedicinesResponse, GetMedicinesAPIOptionalParams } from ".";
import { MedicinesAPIContext } from "./MedicinesAPIContext";

export class MedicinesAPI extends MedicinesAPIContext {
    /**
       * Initializes a new instance of the MedicinesAPI class.
       * @param credentials Subscription credentials which uniquely identify client subscription.
       * @param $host server parameter
       * @param options The parameter options
       */
    constructor(
      credentials: coreAuth.TokenCredential,
      $host: string,
      options?: MedicinesAPIOptionalParams
    ) {
      super(credentials, $host, options);
    }
    getMedicines(options?: GetMedicinesAPIOptionalParams): Promise<GetMedicinesResponse> {
        return this.sendOperationRequest({ options }, getTasksOperationSpec);
      }
}

// Operation Specifications
const serializer = coreClient.createSerializer(Mappers, /* isXml */ false);

const getTasksOperationSpec: coreClient.OperationSpec = {
    path: "/api/Medicines/tasks",
    httpMethod: "GET",
    responses: {
      200: {
        bodyMapper: {
          type: {
            name: "Sequence",
            element: { type: { name: "Composite", className: "TaskDto" } }
          }
        }
      }
    },
    queryParameters: [Parameters.query],
    urlParameters: [Parameters.$host],
    headerParameters: [Parameters.accept],
    serializer
  };