import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";
import MenuButtons from './MenuButtons';

export default function Header() {

    const displayHeader = () => {
        return  <Toolbar>
          {logoApp}
          <MenuButtons/>
        </Toolbar>;
    }

    const logoApp = (
      <Typography variant="h6" component="h1">
          MyMedicinesApp
      </Typography>
    )
    
  return (
    <header>
        <AppBar>{displayHeader()}</AppBar>
    </header>
  );
}

