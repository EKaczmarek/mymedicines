export class Medicine {
    Id!: number;
    Name!: string;
    ExpirationDate!: Date;
    Category!: string;
    Quantity!: number;
    Unit!: string;
}
