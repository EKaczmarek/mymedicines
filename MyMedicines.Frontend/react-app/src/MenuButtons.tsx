import { Button } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";

const headersData = [
    {
      label: "All medicines",
      href: "/medicines",
    },
    {
      label: "Add medicine",
      href: "/add/medicine",
    },
    {
      label: "Log Out",
      href: "/logout",
    },
  ];

   
export default function MenuButtons(){  
      
    const getMenuButtons = () => {
        return headersData.map(({label, href}) => (
            <Button
            {...{
              key: label,
              to: href,
              color: "inherit",
              component: RouterLink
            }}
          >
              {label}
              </Button>
        ));
    };

    return (
        <p>{getMenuButtons()}</p>
    );
}