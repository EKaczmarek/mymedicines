﻿using MyMedicines.Biz.Models;
using MyMedicines.Blazor.Interfaces;
using MyMedicines.Common;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;

namespace MyMedicines.Blazor.Services
{
    /// <summary>
    /// Class provides data from backend to frontend
    /// when source data is on docker volume
    /// </summary>
    public class ParametersService<T> : IParametersService<T> 
    {
        private readonly HttpClient httpClient;

        [Inject] ILocalStorageService localStorage { get; set; }

        public ParametersService(HttpClient client, ILocalStorageService localStorageService)
        {
            httpClient = client;
            localStorage = localStorageService;
        }
        
        public async ValueTask<List<T>>GetParameter<T>(string paramName)
        {
            //var parameters = await localStorage.GetItemAsync<List<T>>(paramName);
            //if (parameters. == null)
            //{
                var parameters = await httpClient.GetAsyncFromBaseAddress<List<T>>(paramName);
                await localStorage.SetItemAsync(paramName, parameters);
            //}

            return parameters;
        }

    }
}
