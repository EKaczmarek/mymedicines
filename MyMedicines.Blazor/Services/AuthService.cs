﻿using MyMedicines.Blazor.Interfaces;

namespace MyMedicines.Blazor.Services
{
    // TODO EK: Not used!
    public class AuthService : IAuthService
    {
        private readonly HttpClient httpClient;

        public AuthService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public ValueTask<string> GetToken(string jsonUser)
        {
            return httpClient.PostAsyncFromBaseAddress<string>("users/authenticate", jsonUser);
        }
    }
}
