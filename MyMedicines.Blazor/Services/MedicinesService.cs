﻿using MyMedicines.Biz.Models;
using MyMedicines.Blazor.Interfaces;
using MyMedicines.Common;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;

namespace MyMedicines.Blazor.Services
{
    /// <summary>
    /// Class provides data from backend to frontend
    /// when source data is on docker volume
    /// </summary>
    public class MedicinesService : IMedicinesService
    {
        private readonly HttpClient httpClient;

        [Inject] ILocalStorageService localStorage { get; set; }

        public MedicinesService(HttpClient client, ILocalStorageService localStorageService)
        {
            httpClient = client;
            localStorage = localStorageService;
        }

        public ValueTask<List<Medicine>> GetAllMedicines()
        {
            return httpClient.GetAsyncFromBaseAddress<List<Medicine>>("Medicines");
        }       

        public async Task SaveMedicine(Medicine medicine)
        {
            await httpClient.PostAsyncFromBaseAddress<Medicine>("Medicines", JsonUtils.ObjectToJson(medicine));
        }

        public async Task PutMedicine(Medicine medicine)
        {
            await httpClient.PutAsyncFromBaseAddress<Medicine>("Medicines", JsonUtils.ObjectToJson(medicine));
        }

        public Task DeleteMedicine(int id)
        {
            throw new NotImplementedException();
        }
    }
}
