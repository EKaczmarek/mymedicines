﻿using MyMedicines.Blazor.Interfaces;

namespace MyMedicines.Blazor.Services
{
    public class ChartsService : IChartService
    {
        private readonly IMedicinesService _medicinesService;

        public ChartsService(IMedicinesService medicinesService)
        {
            _medicinesService = medicinesService;
        }

        public async ValueTask<(string[], List<double>)> GetDataToDiagrams()
        {
            var medicines = (await _medicinesService.GetAllMedicines())
                .OrderBy(x => x.Id);
            var medNames = medicines.Select(x => x.Name).ToArray();
            var medAmounts = medicines.Select(x => (double)x.Quantity).ToList();
            
            return (medNames, medAmounts);
        }

        public async ValueTask<string[]> GetUnitLabels()
        {
            var medicines = await _medicinesService.GetAllMedicines();
            var unitLabels = medicines.OrderBy(x => x.Id).Select(x => x.Unit.Name).ToArray();

            return unitLabels;
        }
    }
}
