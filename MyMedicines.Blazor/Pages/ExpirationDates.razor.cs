﻿using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Models;
using MyMedicines.Blazor.Interfaces;

namespace MyMedicines.Blazor.Pages
{
    /// <summary>
    /// Provides medicines together with expiration dates
    /// </summary>
    public partial class ExpirationDates : ComponentBase
    {
        private static readonly Dictionary<int, string> _months = new()
        {
            { 1, "Styczeń" },
            { 2, "Luty" },
            { 3, "Marzec" },
            { 4, "Kwiecień" },
            { 5, "Maj" },
            { 6, "Czerwiec" },
            { 7, "Lipiec" },
            { 8, "Sierpień" },
            { 9, "Wrzesień" },
            { 10, "Październik" },
            { 11, "Listopad" },
            { 12, "Grudzień" },
        };

        private List<ExpirationDate> medsList = new();

        [Inject] IMedicinesService MedicinesService { get; set; } = null!;

        protected override async Task OnInitializedAsync()
        {
            medsList = (await MedicinesService.GetAllMedicines())
                .ConvertAll(x => new ExpirationDate
                {
                    Date = x.ExpirationDate,
                    Year = x.ExpirationDate.Year,
                    Month = x.ExpirationDate.Month,
                    MedicineName = x.Name,
                });

            await base.OnInitializedAsync();
        }

        /// <summary>
        /// Styles the record if the medicine's date expired
        /// </summary>
        /// <param name="date">Considered date</param>
        /// <param name="styling">Row styling</param>
        private void OnRowStyling(ExpirationDate date, DataGridRowStyling styling)
        {
            if (date.IsExpired())
                styling.Style = "color: red;";
        }

        public static string MonthIdToLabel(int month) => _months[month];
    }
}
