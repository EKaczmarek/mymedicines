using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Security;
using MyMedicines.Blazor.Interfaces;
using MyMedicines.Common;

namespace MyMedicines.Blazor.Pages
{
    /// <summary>
    /// Class provides data to medicines view
    /// </summary>
    public partial class Login : ComponentBase
    {
        private bool _loading;

        private User _model = new User();

        [Inject] IAuthService AuthService { get; set; }

        private async void OnValidSubmit()
        {
            _loading = true;
            try
            {
                var userCred = new UserCred { Username = _model.Login, Password = _model.Password };
                var jsonUser = JsonUtils.ObjectToJson<UserCred>(userCred);
                //var token =
                await AuthService.GetToken(jsonUser);

            }
            catch (Exception ex)
            {
                _loading = false;
                StateHasChanged();
            }
        }
    }
}
