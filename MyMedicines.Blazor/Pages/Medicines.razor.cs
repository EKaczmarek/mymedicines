﻿using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Models;
using MyMedicines.Biz.Services;
using MyMedicines.Blazor.Interfaces;

namespace MyMedicines.Blazor.Pages
{
    /// <summary>
    /// Class provides data to medicines view
    /// </summary>
    public partial class Medicines : ComponentBase
    {
        // Fields passed from backend to component
        private readonly List<Medicine> medicinesList = new();
        private readonly List<Category> categoryList = new();

        public List<IParameter> Categories
        {
            get { return categoryList.Cast<IParameter>().ToList(); }
            set => Categories = categoryList.Cast<IParameter>().ToList();
        }

        [Inject] IMedicinesService MedicinesService { get; set; } = null!;
        [Inject] IParametersService<IParameter> ParametersService { get; set; } = null!;


        protected override async Task OnInitializedAsync()
        {
            medicinesList.AddRange(await MedicinesService.GetAllMedicines());
            categoryList.AddRange(await ParametersService.GetParameter<Category>("Categories"));
        }
        
    }
}
