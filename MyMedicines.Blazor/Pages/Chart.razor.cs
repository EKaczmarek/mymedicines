﻿using Blazorise.Charts;
using Microsoft.AspNetCore.Components;
using MyMedicines.Blazor.Interfaces;

namespace MyMedicines.Blazor.Pages
{
    public partial class Chart : ComponentBase
    {
        private BarChart<double> _barChart = new();

        [Inject] IChartService Service { get; set; } = null!;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await HandleRedraw();
            }
        }

        private async Task HandleRedraw()
        {
            var (labels, datasets) = await Service.GetDataToDiagrams();

            await _barChart.Clear();
            await _barChart.AddLabelsDatasetsAndUpdate(labels, GetBarChartDataset(datasets));
        }

        private static BarChartDataset<double> GetBarChartDataset(List<double> dataset)
        {
            return new BarChartDataset<double>
            {
                Label = "ilość", 
                Data = dataset,
                BackgroundColor = GetBackgroundColours(dataset.Count, 0.2f),
                BorderColor = GetBackgroundColours(dataset.Count, 1f),
            };
        }

        private static List<string> GetBackgroundColours(int numberOfColours, float alpha)
        {
            var random = new Random();
            var colours = new List<string>();
            for (int i = 0; i < numberOfColours; i++)
            {
                var colour = ChartColor.FromRgba((byte)random.Next(0,255), (byte)random.Next(0, 255), (byte)random.Next(0, 255), alpha);
                colours.Add(colour);
            }
            return colours;
        }        
    }
}
