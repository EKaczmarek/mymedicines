﻿using Blazorise;
using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Extensions;
using MyMedicines.Biz.Models;
using MyMedicines.Blazor.Interfaces;
using System.Diagnostics;

namespace MyMedicines.Blazor.Pages
{
    /// <summary>
    /// Class manages adding new medicine 
    /// to the repository
    /// </summary>
    public partial class NewMedicine : ComponentBase 
    {
        private int categoryId;
        private int unitId;

        private readonly List<Unit> units = new();
        private readonly List<Category> categories = new();

        private Validations validations = new();

        private readonly Medicine medicine = new();

        [Inject] IMedicinesService MedicinesService { get; set; } = null!;

        [Inject] IParametersService<IParameter> ParametersService { get; set; } = null!;

        protected override async Task OnInitializedAsync()
        {
            categories.AddRange(await ParametersService.GetParameter<Category>("Categories"));
            units.AddRange(await ParametersService.GetParameter<Unit>("Units"));

            Debug.Assert(categories.Count > 0, "There should be something! Check DB");
            Debug.Assert(units.Count > 0, "There should be something! Check DB");
        }

        private async Task Submit()
        {
            if (!await validations.ValidateAll())
                return;
            
            var unit = units.GetObjectById(unitId);
            var category = categories.GetObjectById(categoryId);

            // TODO EK: Logging! Rzucanie tutaj wyjątku jest słabe dla usera!
            if (unit is null) throw new ArgumentException($"Illegal unit ID: {unit}");
            if (category is null) throw new ArgumentException($"Illegal category ID: {category}");  

            medicine.Unit = unit;
            medicine.Category = category;
            await MedicinesService.SaveMedicine(medicine);
        }
    }
}
