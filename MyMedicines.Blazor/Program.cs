using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MyMedicines.Blazor;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using MyMedicines.Blazor.Services;
using MyMedicines.Blazor.Interfaces;
using Blazored.LocalStorage;
using MyMedicines.Biz.Models;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

// Register services
builder.Services
    .AddBlazorise(options =>
    {
        options.ChangeTextOnKeyPress = true;
    })
    .AddBootstrapProviders()
    .AddFontAwesomeIcons();
builder.Services.AddScoped<IMedicinesService, MedicinesService>();
builder.Services.AddScoped<IParametersService<IParameter>, ParametersService<IParameter>>();
builder.Services.AddScoped<IChartService, ChartsService>();
builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddBlazoredLocalStorage();

// Register components
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");


var backendUrl = builder.Configuration.GetValue<string>("Settings:BackendUrl");
builder.Services.AddTransient(sp => new HttpClient { BaseAddress = new Uri(backendUrl) });

// Run app
await builder.Build().RunAsync();
