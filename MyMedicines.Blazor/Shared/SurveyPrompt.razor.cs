﻿using Microsoft.AspNetCore.Components;

namespace MyMedicines.Blazor.Shared
{
    public partial class SurveyPrompt : ComponentBase
    {
        [Parameter]
        public string? Title { get; set; }
    }
}
