﻿using MyMedicines.Blazor.Extensions;
using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Models;
using MyMedicines.Biz.Extensions;

namespace MyMedicines.Blazor.Shared
{
    public partial class SelectCategory: ComponentBase
    {
        [Parameter]
        public List<IParameter>? Data { get; set; }

        [Parameter]
        public string DataLabel { get; set; }

        private Task OnChange(int dataId)
        {
            if (Data?.FirstOrDefault() is Category)
            {
                var category = Data.GetObjectById(dataId);
                Mediator.GetInstance().OnCategoryChanged(this, category);
                Cat = category.Id;
                CatChanged.InvokeAsync(Cat);
            }

            else if (Data?.FirstOrDefault() is Unit)
            {
                var unit = Data.GetObjectById(dataId);
                Un = unit.Id;
                UnChanged.InvokeAsync(Un);
            }

            return Task.CompletedTask;
        }

        [Parameter]
        public int Cat { get; set; }

        [Parameter]
        public EventCallback<int> CatChanged { get; set; }


        [Parameter]
        public int Un { get; set; }

        [Parameter]
        public EventCallback<int> UnChanged { get; set; }

    }
}
