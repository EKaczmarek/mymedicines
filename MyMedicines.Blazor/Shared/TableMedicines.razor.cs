﻿using MyMedicines.Blazor.Extensions;
using Microsoft.AspNetCore.Components;
using MyMedicines.Biz.Models;
using MyMedicines.Biz.Extensions;

namespace MyMedicines.Blazor.Shared
{
    public partial class TableMedicines : ComponentBase
    {
        [Parameter]
        public List<Medicine>? myMedicines { get; set; }

        public List<Medicine>? _allMedicines;

        protected override void OnParametersSet()
        {
            if (myMedicines != null)
                _allMedicines = myMedicines;
        }

        protected override void OnInitialized()
        {
            Mediator.GetInstance().CategoryChanged += (s, e) => BindData(e.Category);
        }

        private void BindData(Category category)
        {
            if (category != null)
            {
                myMedicines = _allMedicines?.GetMedicinesByCategory(category);
            }
            else
            {
                myMedicines = _allMedicines;
            }

            StateHasChanged();
        }
    }
}
