﻿using Newtonsoft.Json;
using System.Text;

namespace System.Net.Http
{
    public static class HttpClientUtils
    {
        public async static ValueTask<T> GetAsyncFromBaseAddress<T>(this HttpClient client, string end)
        {
            //client.DefaultRequestHeaders.Authorization =
            //  new AuthenticationHeaderValue("Bearer", "");
            var endpoint = $"{client.BaseAddress}{end}";
            var response = await client.GetAsync(endpoint);
            var content = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<T>(content);
            return data;
        }

        public static async ValueTask<T> GetAsync<T>(this HttpClient client, string address)
        {
            var response = await client.GetAsync(address);
            var content = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<T>(content);
            return data;
        }

        public static async ValueTask<T> PostAsyncFromBaseAddress<T>(this HttpClient client, string address, string jsonUser)
        {
            var body = new StringContent(jsonUser, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{client.BaseAddress}{address}", body);
            var content = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<T>(content);
            return data;
        }

        public static async ValueTask<T> PutAsyncFromBaseAddress<T>(this HttpClient client, string address, string jsonUser)
        {
            var body = new StringContent(jsonUser, Encoding.UTF8, "application/json");
            var response = await client.PutAsync($"{client.BaseAddress}{address}", body);
            var content = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<T>(content);
            return data;
        }
    }
}
