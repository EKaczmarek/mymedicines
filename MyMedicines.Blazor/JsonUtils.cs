﻿using MyMedicines.Blazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Json;

namespace MyMedicines.Common
{
    public static class JsonUtils
    {
        public static string ObjectToJson<T>(T valueObject)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return JsonConvert.SerializeObject(valueObject, serializerSettings);
        }
    }
}
