﻿
using MyMedicines.Biz.Models;

namespace MyMedicines.Blazor.Extensions
{
    /// <summary>
    /// Class to manage changing category 
    /// based on event handler
    /// </summary>
    public sealed class Mediator
    {
        //Singleton functionality
        private static readonly Mediator _instance = new Mediator();
        private Mediator() { }
        public static Mediator GetInstance() { return _instance; }

        //Instance functionality
        public event EventHandler<CategoryChangedEventArgs>? CategoryChanged;

        public void OnCategoryChanged(object sender, IParameter c)
        {
            var categoryChangeDelegate = CategoryChanged;
            if (categoryChangeDelegate != null)
            {
                categoryChangeDelegate(sender, new CategoryChangedEventArgs { Category = (Category)c });
            }

        }
    }
}


