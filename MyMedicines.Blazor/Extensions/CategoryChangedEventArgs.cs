﻿

using MyMedicines.Biz.Models;

namespace MyMedicines.Blazor.Extensions
{
    /// <summary>
    /// Event arguments related to Category 
    /// </summary>
    public class CategoryChangedEventArgs : EventArgs
    {
        public Category? Category { get; set; }
    }
}