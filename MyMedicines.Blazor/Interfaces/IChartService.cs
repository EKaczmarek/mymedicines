﻿namespace MyMedicines.Blazor.Interfaces
{
    public interface IChartService
    {
        public ValueTask<(string[], List<double>)> GetDataToDiagrams();

        public ValueTask<string[]> GetUnitLabels();
    }
}
