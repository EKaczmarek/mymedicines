﻿using MyMedicines.Biz.Models;

namespace MyMedicines.Blazor.Interfaces
{
    /// <summary>
    /// Interface provides data about medicines 
    /// to frontend
    /// </summary>
    public interface IMedicinesService
    {
        ValueTask<List<Medicine>> GetAllMedicines();

        public Task SaveMedicine(Medicine medicine);

        public Task PutMedicine(Medicine medicine);

        public Task DeleteMedicine(int id);
    }
}
