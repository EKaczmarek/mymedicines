﻿using MyMedicines.Biz.Models;

namespace MyMedicines.Blazor.Interfaces
{
    public interface IParametersService<T>
    {
        ValueTask<List<T>> GetParameter<T>(string paramName);

    }
}
