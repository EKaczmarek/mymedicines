using MyMedicines.Biz.Models;
using MyMedicines.Common;
using Xunit;

namespace MyMedicines.Blazor.Tests
{
    public class JsonUtilsTests
    {
        [Fact]
        public void ChangeObjectToJsonTest()
        {
            // Arrange
            var category = new Category() { Id = 1, Name = "Przeziębienie" };
            var expected = "{\"id\":1,\"name\":\"Przeziębienie\"}";

            // Act
            var actual = JsonUtils.ObjectToJson(category);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}