# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY ["MyMedicines.Backend/MyMedicines.Backend.csproj", "MyMedicines.Backend/"]
COPY ["MyMedicines.Biz/MyMedicines.Biz.csproj", "MyMedicines.Biz/"]
COPY ["MyMedicines.Common/MyMedicines.Common.csproj", "MyMedicines.Common/"]
RUN dotnet restore "MyMedicines.Backend/MyMedicines.Backend.csproj"

# Copy everything else and build
COPY ["MyMedicines.Backend/*", "MyMedicines.Backend/"]
COPY ["MyMedicines.Common/*", "MyMedicines.Common/"]
COPY ["MyMedicines.Biz/*", "MyMedicines.Biz/"]
RUN dotnet publish "MyMedicines.Backend" --no-restore -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "MyMedicines.Backend.dll"]