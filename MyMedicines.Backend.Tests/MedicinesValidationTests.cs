using MyMedicines.Backend.Services;
using MyMedicines.Biz.Models;
using System;
using Xunit;

namespace MyMedicines.Backend.Tests
{
    public class MedicinesValidationTests
    {
        public MedicinesValidationTests()
        {

        }

        [Fact]
        public void ValidMedicineTest()
        {
            // Arrange
            var medicine = new Medicine
            {
                Name = "Strepsils",
                Quantity = 6,
                Category = new Category { Id = 1, Name = "Przeziębienie" },
                Unit = new Unit { Id = 1, Name = "tabletka" },
                ExpirationDate = DateTime.Now
            };

            // Act
            var result = MedicineUtils.IsValid(medicine);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void InvalidNameTest()
        {
            // Arrange
            var medicine = new Medicine
            {
                Name = "dw",
                Quantity = 3,
                Category = new Category { Name = "Przeziębienie" },
                Unit = new Unit { Name = "tabletka" },
                ExpirationDate = DateTime.Now
            };

            // Act
            var result = MedicineUtils.IsValid(medicine);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void InvalidQuantityTest()
        {
            // Arrange
            var medicine = new Medicine
            {
                Name = "medicine",
                Quantity = -8,
                Category = new Category { Name = "Przeziębienie" },
                Unit = new Unit { Name = "tabletka" },
                ExpirationDate = DateTime.Now
            };

            // Act
            var result = MedicineUtils.IsValid(medicine);

            // Assert
            Assert.False(result);
        }


        [Fact]
        public void InvalidUnitValueNullTest()
        {
            // Arrange
            var medicine = new Medicine
            {
                Name = "medicine",
                Quantity = 45,
                Category = new Category { Name = "Przeziębienie" },
                ExpirationDate = DateTime.Now
            };

            // Act
            var result = MedicineUtils.IsValid(medicine);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void InvalidUnitAndCategoryNameTest()
        {
            // Arrange 
            var medicine = new Medicine
            {
                Name = "medicine",
                Quantity = 45,
                Category = new Category { Name = "" },
                Unit = new Unit { Name = "" },
                ExpirationDate = DateTime.Now
            };

            // Act
            var result = MedicineUtils.IsValid(medicine);

            // Assert
            Assert.False(result);
        }
    }
}