using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MyMedicines.Backend.Interfaces;
using MyMedicines.Backend.Services;
using MyMedicines.Biz.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MyMedicines.Backend.Tests
{
    public class MedicinesServiceTests
    {
        public MedicinesServiceTests()
        {

        }

        [Fact]
        public async void AddingNewMedicineTest()
        {
            //Arrange            
            var dbContext = await GetDatabaseContext();
            using var medicinesService = new MedicinesService(dbContext);

            // Act
            var medicine = new Medicine
            {
                Name = "Strepsils",
                Quantity = 3,
                Category = new Category { Name = "Przeziębienie" },
                Unit = new Unit { Name = "tabletka" },
                ExpirationDate = DateTime.Now
            };
            await medicinesService.AddMedicine(medicine);

            // Assert
            Assert.Equal(2, dbContext.Medicines.Count());
            Assert.Single(dbContext.Categories);
            Assert.Single(dbContext.Units);
        }

        [Fact]
        public async void GetMedcinesTest()
        {
            //Arrange            
            var dbContext = await GetDatabaseContext();
            using var medicinesService = new MedicinesService(dbContext);

            // Act
            var medicines = await medicinesService.GetMedicines();

            //Assert
            Assert.Single(medicines);
        }

        [Fact]
        public async void DeleteMedcineTest()
        {
            //Arrange            
            var dbContext = await GetDatabaseContext();
            using var medicinesService = new MedicinesService(dbContext);

            // Act
            var medicine = await medicinesService.GetMedicineById(1);
            if (medicine == null)
            {
                return;
            }

            await medicinesService.DeleteMedicine(medicine);
            var medicines = medicinesService.DeleteMedicine(medicine);

            //Assert
            Assert.Empty(dbContext.Medicines);
            Assert.Single(dbContext.Categories);
            Assert.Single(dbContext.Units);
        }

        [Fact]
        public async void PutMedcineTest()
        {
            //Arrange            
            var dbContext = await GetDatabaseContext();
            using var medicinesService = new MedicinesService(dbContext);

            // Act
            var medicine = new Medicine()
            {
                Id = 1,
                Name = "Strepsils",
                Quantity = 10
            };

            var expected = new Medicine()
            {
                Id = 1,
                Name = "Strepsils",
                Quantity = 10,
                Category = new Category { Id = 1, Name = "Przeziębienie" },
                Unit = new Unit { Id = 1, Name = "tabletka" },
                ExpirationDate = new DateTime(2022, 12, 31)
            };

            await medicinesService.UpdateMedicine(medicine);
            var actualMedicine = await medicinesService.GetMedicineById(1);

            //Assert
            Assert.Equal(expected, actualMedicine);
        }

        private async Task<MedicineContext> GetDatabaseContext()
        {
            var options = new DbContextOptionsBuilder<MedicineContext>()
                .EnableSensitiveDataLogging()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var databaseContext = new MedicineContext(options);
            databaseContext.Database.EnsureCreated();

            databaseContext.Medicines.Add(new Medicine()
            {
                Name = "Strepsils",
                Quantity = 7,
                Category = new Category { Name = "Przeziębienie" },
                Unit = new Unit { Name = "tabletka" },
                ExpirationDate = new DateTime(2022, 12, 31)
        });
            await databaseContext.SaveChangesAsync();

            return databaseContext;
        }
    }
}