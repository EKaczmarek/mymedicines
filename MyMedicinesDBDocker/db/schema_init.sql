CREATE TABLE units (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE categories (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE medicines (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  expiration_date DATE NOT NULL,
  quantity INTEGER NOT NULL,
  
  category_id INTEGER,
  CONSTRAINT fk_category
      FOREIGN KEY(category_id) 
      REFERENCES categories(id),
  unit_id INTEGER,
  CONSTRAINT fk_unit
      FOREIGN KEY(unit_id) 
      REFERENCES units(id) 
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  login TEXT NOT NULL,
  password TEXT NOT NULL,
  jw_token TEXT );

INSERT INTO units VALUES (1, 'tabletka');
INSERT INTO units VALUES (2, 'ml');

INSERT INTO categories VALUES (1, 'Ból głowy');
INSERT INTO categories VALUES (2, 'Przeziębienie');
INSERT INTO categories VALUES (3, 'Suplement');
INSERT INTO categories VALUES (4, 'Suplement - magnez i potas');
INSERT INTO categories VALUES (5, 'Opryszczka');

INSERT INTO medicines VALUES (1, 'Strepsils', '2021-12-12T00:00:00+01:00', 10, 2, 1);
INSERT INTO medicines VALUES (2, 'Apap', '2022-01-06T17:16:40+01:00', 10, 1, 1);
INSERT INTO medicines VALUES (3, 'Heviran', '2023-01-01T00:00:00+01:00', 7, 5, 1);
INSERT INTO medicines VALUES (4, 'Witamina D3', '2022-01-01T00:00:00+01:00', 8, 3, 1);
INSERT INTO medicines VALUES (5, 'Skrzyp polny', '2021-12-12T00:00:00+01:00', 25, 4, 1);
INSERT INTO medicines VALUES (6, 'Amol', '2021-12-12T00:00:00+01:00', 10, 1, 2);

INSERT INTO users VALUES (1, 'ela', '2jonN7v33cMAvv/2Z6Uu0R+V95oP9S8k1wiQ1sdSPD0=');
INSERT INTO users VALUES (2, 'rafa', '2jonN7v33cMAvv/2Z6Uu0R+V95oP9S8k1wiQ1sdSPD0=');