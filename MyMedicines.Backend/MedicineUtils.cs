﻿using Microsoft.EntityFrameworkCore;
using MyMedicines.Biz.Models;

namespace MyMedicines.Backend.Services
{
    public static class MedicineUtils
    {

        public static bool IsValid(Medicine medicine)
        {
            if (medicine.Name.Count() < 3)
                return false;

            if (!medicine.Category.IsParameterValid<Category>() || !medicine.Unit.IsParameterValid<Unit>())
                return false;

            if (medicine.Quantity < 0)
                return false;

            return true;

        }
        internal static bool IsParameterValid<T>(this T parameter) where T : IParameter
        {
            if (parameter?.Name is null || parameter?.Name?.Length == 0)
                return false;

            return true;
        }
    }
}