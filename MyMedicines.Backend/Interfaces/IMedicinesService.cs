﻿using Microsoft.AspNetCore.Mvc;
using MyMedicines.Biz.Models;

namespace MyMedicines.Backend.Interfaces
{
    /// <summary>
    /// Service for executing specific REST methods
    /// added becuase of possible different source of medicines
    /// </summary>
    public interface IMedicinesService
    {
        Task<List<Medicine>> GetMedicines();

        Task<Medicine?> GetMedicine(long id);

        Task AddMedicine(Medicine medicine);

        Task DeleteMedicine(Medicine medicine);

        Task UpdateMedicine(Medicine medicine);

        Task<Medicine?> GetMedicineById(int id);
    }
}
