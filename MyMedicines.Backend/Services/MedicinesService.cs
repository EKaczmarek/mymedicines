﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyMedicines.Backend.Interfaces;
using MyMedicines.Biz.Models;

namespace MyMedicines.Backend.Services
{
    /// <summary>
    /// Service used to handle data objects based on db context 
    /// </summary>
    public class MedicinesService : IMedicinesService, IDisposable
    {
        private readonly MedicineContext _context;

        public MedicinesService(MedicineContext context)
        {
            _context = context;
        }

        public MedicinesService()
        {

        }

        public Task<Medicine?> GetMedicine(long id)
        {
            return _context.Medicines
                .Include(u => u.Unit)
                .Include(c => c.Category)
                .Where(i => i.Id == id)
                .SingleOrDefaultAsync();
        }

        public Task<List<Medicine>> GetMedicines()
        {
            return _context.Medicines
                .Include(u => u.Unit)
                .Include(c => c.Category)
                .ToListAsync();
        }

        public async Task AddMedicine(Medicine medicine)
        {
            medicine.ExpirationDate = medicine.ExpirationDate.SetKindUtc();

            if (MedicineUtils.IsValid(medicine))
            {
                _context.Medicines.Add(medicine);
                _context.Entry(medicine.Category).State = EntityState.Unchanged;
                _context.Entry(medicine.Unit).State = EntityState.Unchanged;

                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteMedicine(Medicine medicine)
        {
            _context.Medicines.Remove(medicine);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMedicine(Medicine medicine)
        {
            var result = await _context.Medicines.SingleOrDefaultAsync(m => m.Id == medicine.Id);
            if (result != null)
            {
                result.Quantity = medicine.Quantity;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<Medicine?> GetMedicineById(int id)
        {
            return await _context.Medicines.FindAsync(id);
        }


        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
