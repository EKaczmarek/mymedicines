﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MyMedicines.Backend.Interfaces;
using MyMedicines.Backend.Services;
using MyMedicines.Biz.Services;
using MyMedicines.Common.Security;
using System.Text;

namespace MyMedicines.Backend
{
    public static class ServicesConfiguration
    {
        public static IConfiguration Configuration { get; }

        private static void ConfigurePostgresSQL(IServiceCollection services)
        {
            services.AddDbContext<MedicineContext>(options =>
                        options
                            .UseNpgsql(Configuration["ConnectionStrings:MedicinesDatabase"])
                            .UseSnakeCaseNamingConvention()
                            .EnableSensitiveDataLogging()); 
        }
        private static void ConfigureInMemory(IServiceCollection services)
        {
            services.AddDbContext<MedicineContext>(options =>
                        options
                            .UseInMemoryDatabase(databaseName: "FakeMedicinesDbContext"));
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public static void AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var databaseSection = configuration.GetSection("ConnectionStrings:MedicinesDatabase").Value;

            if (databaseSection == "")
                ConfigureInMemory(services);
            else
                ConfigurePostgresSQL(services);

            services
                .AddCors(options =>
                {
                    options.AddDefaultPolicy(builder =>
                    builder.WithOrigins("https://localhost:7157")
                    .AllowAnyHeader()
                    .AllowAnyMethod());
                })
                .AddControllers();

            services.AddEndpointsApiExplorer();

            services.AddSwaggerGen()
                    .AddDbContext<MedicineContext>()
                    .AddMvc()
                    .AddControllersAsServices();

            var tokenKey = PasswordUtils.RandomString(32);
            var key = Encoding.ASCII.GetBytes(tokenKey);

            services
                .AddAuthentication()
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddSingleton<IJWTAuthenticationManager>(new JWTAuthenticationManager(tokenKey));
            services.AddScoped<IMedicinesService, MedicinesService>();
            services.AddTransient<MedicineDataSeeder>();
        }
    }
}
