﻿using MyMedicines.Biz.Models;

namespace MyMedicines.Backend
{
    public class MedicineDataSeeder
    {
        private readonly MedicineContext _medicinesDbContext;

        public MedicineDataSeeder(MedicineContext medicinesDbContext)
        {
            _medicinesDbContext = medicinesDbContext;
        }

        public void Seed()
        {
            SeedWithCategories();
            SeedWithUnits();
            SeedWithMedicines();
        }

        public void SeedWithCategories()
        {
            if (!_medicinesDbContext.Categories.Any())
            {
                var categories = new List<Category>()
                {
                        new Category()
                        {
                            Id = 1,
                            Name = "Ból gardła"
                        },
                        new Category()
                        {
                            Id = 2,
                            Name = "Ból głowy"
                        }
                };

                _medicinesDbContext.Categories.AddRange(categories);
                _medicinesDbContext.SaveChanges();
            }
        }
        public void SeedWithUnits()
        {
            if (!_medicinesDbContext.Units.Any())
            {
                var units = new List<Unit>()
                {
                        new Unit()
                        {
                            Id = 1,
                            Name = "tabletka"
                        },
                        new Unit()
                        {
                            Id = 2,
                            Name = "ml"
                        }
                };

                _medicinesDbContext.Units.AddRange(units);
                _medicinesDbContext.SaveChanges();
            }
        }
        public void SeedWithMedicines()
        {
            if (!_medicinesDbContext.Medicines.Any())
            {
                var medicines = new List<Medicine>() {
                    new Medicine()
                          {
                              Id = 1,
                              Name = "Apap",
                              Quantity = 2,
                              ExpirationDate = DateTime.Now,
                              Unit = _medicinesDbContext.Units.FirstOrDefault(u=>u.Id ==1),
                              Category = _medicinesDbContext.Categories.FirstOrDefault(u=>u.Id ==2)
                          },
                    new Medicine()
                    {
                              Id = 2,
                              Name = "Strepsils",
                              Quantity = 10,
                              ExpirationDate = DateTime.Parse("01-01-2025"),
                              Unit = _medicinesDbContext?.Units?.FirstOrDefault(u=>u.Id ==1),
                              Category = _medicinesDbContext?.Categories?.FirstOrDefault(u=>u.Id ==1)                    
                    }
                };

                _medicinesDbContext.Medicines.AddRange(medicines);
                _medicinesDbContext.SaveChanges();
            }
        }
    }
}
