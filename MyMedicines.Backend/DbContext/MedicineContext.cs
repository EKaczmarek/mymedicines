﻿using Microsoft.EntityFrameworkCore;
using MyMedicines.Backend.Services;
using MyMedicines.Biz.Models;
using MyMedicines.Biz.Security;

namespace MyMedicines.Backend
{
    public class MedicineContext : DbContext
    {

        public MedicineContext(DbContextOptions<MedicineContext> options)
            : base(options)
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<User> Users { get; set; }
    }

}
