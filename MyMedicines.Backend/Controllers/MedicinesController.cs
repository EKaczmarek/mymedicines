﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyMedicines.Backend.Interfaces;
using MyMedicines.Biz.Models;

namespace MyMedicines.Backend.Controllers
{
    /// <summary>
    /// Returning specific RESTful responses
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MedicinesController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IMedicinesService _medicinesService;
        private readonly ILogger<MedicinesController> _logger;

        public MedicinesController(
            IConfiguration configuration,
            IMedicinesService medicinesService,
            ILogger<MedicinesController> logger)
        {
            _configuration = configuration;
            _medicinesService = medicinesService;
            _logger = logger;
        }

        // GET: api/Medicines
        [HttpGet]
        public async Task<List<Medicine>> GetMedicines()
        {
            try
            {
                return await _medicinesService.GetMedicines();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "HTTP GET error: Could not load medicines");
                throw;
            }
        }

        // GET: api/Medicines/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Medicine>> GetMedicine(long id)
        {
            try
            {
                var medicine = await _medicinesService.GetMedicine(id);

                if (medicine == null)
                {
                    return NotFound();
                }

                return Ok(medicine);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"HTTP GET error: Could not get medicine with id {id}");
                throw;
            }
        }

        // POST: api/Medicines
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Medicine>> PostMedicine(Medicine medicine)
        {
            try
            {
                await _medicinesService.AddMedicine(medicine);

                return CreatedAtAction(
                    nameof(GetMedicine),
                    new { id = medicine.Id },
                    medicine
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"HTTP POST error: Could not add new medicine");
                throw;
            }
        }

        // DELETE: api/Medicines/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicine(int id)
        {
            try
            {
                var medicine = await _medicinesService.GetMedicineById(id);
                if (medicine == null)
                {
                    return NotFound();
                }

                await _medicinesService.DeleteMedicine(medicine);

                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"HTTP GET error: Could not delete medicine with id {id}");
                throw;
            }
        }

        // PUT api/Medicines/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Medicine>> Put(int id, Medicine medicine)
        {
            try
            {
                if (id != medicine.Id)
                    return BadRequest("Employee ID mismatch");

                var med = await _medicinesService.GetMedicineById(id);

                if (med == null)
                    return NotFound($"Medicine with Id = {id} not found");

                await _medicinesService.UpdateMedicine(medicine);
                return AcceptedAtAction(
                    nameof(GetMedicine),
                    new { id = medicine.Id },
                    medicine
                ); 
                
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"HTTP GET error: Could not update medicine with id {id}");
                throw;
            }
        }


    }
}
