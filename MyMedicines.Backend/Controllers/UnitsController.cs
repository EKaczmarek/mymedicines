﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyMedicines.Biz.Models;

namespace MyMedicines.Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitsController : ControllerBase
    {
        private readonly MedicineContext _context;

        public UnitsController(MedicineContext context)
        {
            _context = context;
        }

        // GET: api/Units
        [HttpGet]
        public IEnumerable<Unit> GetUnits()
        {
            return _context.Units;
        }

        // GET: api/Units/przeziebienie
        [HttpGet("{name}")]
        public async Task<ActionResult<Unit>> GetUnit(string name)
        {
            var unit = await _context.Units.Where(x => x.Name == name).FirstOrDefaultAsync();

            return unit ?? (ActionResult<Unit>)NotFound();
        }

        // POST: api/Units
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Unit>> PostUnit(Unit unit)
        {
            _context.Units.Add(unit);
            await _context.SaveChangesAsync();

            return Ok(unit);
        }

        // DELETE: api/Units/5        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUnit(int id)
        {
            var unit = await _context.Units.FindAsync(id);
            if (unit == null)
            {
                return NotFound();
            }

            _context.Units.Remove(unit);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}