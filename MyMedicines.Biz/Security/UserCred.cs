﻿namespace MyMedicines.Biz.Security
{
    /// <summary>
    /// Class provides template for user's authentication
    /// </summary>
    public class UserCred
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
    }
}
