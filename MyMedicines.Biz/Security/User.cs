﻿namespace MyMedicines.Biz.Security
{
    /// <summary>
    /// Provides user's properties like in the database,
    /// containes jwt token property 
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }
        public string? JwToken { get; set; }
    }
}