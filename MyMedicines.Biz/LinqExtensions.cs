﻿using MyMedicines.Biz.Models;

namespace MyMedicines.Biz.Extensions
{

    /// <summary>
    /// Class provides results of query in LINQ
    /// </summary>
    public static class LinqExtensions
    {
        /// <summary>
        ///     Method returns medicines with specific category
        /// </summary>
        /// <typeparam name="TMed">Medicine class template</typeparam>
        /// <typeparam name="TPar">Parameter of medicine class that includes Category, Unit</typeparam>
        /// <param name="list">List of medicines to filter data</param>
        /// <param name="param">Category parameter</param>
        /// <returns></returns>
        public static List<TMed> GetMedicinesByCategory<TMed, TPar>(this List<TMed> list, TPar param) where TMed : Medicine where TPar : IParameter
        {
            return list.Where(c => c.Category?.Id == param.Id).ToList();
        }
        /// <summary>
        ///     Method returns object with a specific id
        /// </summary>
        /// <typeparam name="T">Category or Medicine (classes that implements IParameter interface)</typeparam>
        /// <param name="list">List of entities to filter data</param>
        /// <param name="id">Entity's id </param>
        /// <returns></returns>
        public static T? GetObjectById<T>(this List<T> list, int id) where T : IParameter
        {
            if (id == 0)
                return default;
            else
                return list.Where(c => c.Id == id).First();
        }
    }
}
