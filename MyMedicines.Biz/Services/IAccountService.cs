﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMedicines.Biz.Models;
using MyMedicines.Biz.Security;

namespace MyMedicines.Biz.Services
{
    public interface IAccountService
    {
        User User { get; }
        Task Initialize();
        Task Login(User model);
        Task Logout();
    }
}
