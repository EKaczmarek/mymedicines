﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MyMedicines.Biz.Security;
using MyMedicines.Common.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MyMedicines.Biz.Services
{
    /// <summary>
    /// Implementation of user's authentication
    /// </summary>
    public class JWTAuthenticationManager : IJWTAuthenticationManager
    {
        IDictionary<string, string?> _users = new Dictionary<string, string?> { };

        private readonly string tokenKey;

        public JWTAuthenticationManager(string tokenKey)
        {
            this.tokenKey = tokenKey;
        }

        /// <summary>
        /// User's Basic authentication method
        /// </summary>
        /// <param name="username">user's login</param>
        /// <param name="password">user's password</param>
        /// <returns></returns>
        public string? Authenticate(string username, string password)
        {
            if (!_users.Any(u => u.Key == username
                && u.Value == PasswordUtils.GetHashedPassword(password)))
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(tokenKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, username)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenKey)),
                    SecurityAlgorithms.HmacSha256)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public void SetUsersDictionary(ActionResult<IEnumerable<User>> users)
        {
            _users = users.Value
                           .Select(u => (u.Login, u.Password))
                           .Where(x => !string.IsNullOrWhiteSpace(x.Login))
                           .ToDictionary(x => x.Login!, x => x.Password);
        }
    }

}
