﻿using Microsoft.AspNetCore.Mvc;
using MyMedicines.Biz.Security;

namespace MyMedicines.Biz.Services
{
    /// <summary>
    /// Interface provides methods to authenticate user
    /// </summary>
    public interface IJWTAuthenticationManager
    {
        string? Authenticate(string username, string password);
        void SetUsersDictionary(ActionResult<IEnumerable<User>> users);
    }
}
