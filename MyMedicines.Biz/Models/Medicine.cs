﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMedicines.Biz.Models
{
    /// <summary>
    /// Manages medicines in the home pharmacy
    /// </summary>
    public class Medicine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public Category Category { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public Unit Unit { get; set; }

        public Medicine()
        {
            Category = new Category();
            Unit = new Unit();
        }

        public override bool Equals(object? obj)
        {
            var med = obj as Medicine;

            if (med == null)
                return false;

            if (med.Id != Id)
                return false;

            if (med.Name != Name)
                return false;

            if (med.Category.Id != med.Category.Id)
                return false;

            if (med.Category.Name != med.Category.Name)
                return false;

            if (med.Unit.Id != med.Unit.Id)
                return false;

            if (med.Unit.Name != med.Unit.Name)
                return false;

            if (med.ExpirationDate.ToString() != med.ExpirationDate.ToString())
                return false;

            if (med.Quantity != Quantity)
                return false;

            return true;

        }
    }
}
