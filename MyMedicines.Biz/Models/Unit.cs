﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMedicines.Biz.Models
{
    /// <summary>
    /// Manages units for medicines
    /// </summary>
    public class Unit : IParameter
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
