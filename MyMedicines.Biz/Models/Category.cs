﻿
using System.ComponentModel.DataAnnotations;

namespace MyMedicines.Biz.Models
{
    /// <summary>
    /// Manages categories for medicines 
    /// </summary>
    public class Category : IParameter
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
