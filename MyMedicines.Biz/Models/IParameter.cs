﻿namespace MyMedicines.Biz.Models
{
    /// <summary>
    /// Interface that provides medicines' parameters like category, unit
    /// (in the future may be more)
    /// </summary>
    public interface IParameter
    {
        int Id { get; }
        string Name { get; }
    }
}