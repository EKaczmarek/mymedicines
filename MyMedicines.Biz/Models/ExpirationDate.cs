﻿namespace MyMedicines.Biz.Models
{
    /// <summary>
    /// Class that provides conversion 
    /// between month number and month name
    /// and expression body method that check
    /// if expiration date is in the past
    /// </summary>
    public class ExpirationDate
    {
        public int Month { get; set; }

        public int Year { get; set; }

        public string MedicineName { get; set; } = string.Empty;

        public DateTime Date { get; set; }

        public bool IsExpired() => DateTime.Now >= Date;
    }
}
